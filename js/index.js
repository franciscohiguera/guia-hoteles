$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carrousel').carousel(
        {
            interval:1000
        }
    )


    /*
  $('#contactoBtn').modal({
      show.bs.modal
  })*/

    $('#contacto').on('show.bs.modal', function (e) {
        console.log("El modal contacto se está mostrando");
        $('#contactoBtn').removeClass('btn-success');
        $('#contactoBtn').addClass('btn-primary');
        $('#contactoBtn').prop('disable', true);
    });
    $('#contacto').on('shown.bs.modal', function (e) {
        console.log("El modal contacto se mostró");
    });
    $('#contacto').on('hide.bs.modal', function (e) {
        console.log("El modal contacto se está ocultando");
    });
    $('#contacto').on('hidden.bs.modal', function (e) {
        console.log("El modal contacto se ocultó");
        $('#contactoBtn').removeClass('btn-primary');
        $('#contactoBtn').addClass('btn-success');
        $('#contactoBtn').prop('disable', false);

    });
})