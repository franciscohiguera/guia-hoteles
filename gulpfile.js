// Por efectos de version de gulp utilizada, se realizo lo siguiente:

const {
    src,
    dest,
    parallel,
    series,
    watch
} = require('gulp');

// Load plugins

const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const cssnano = require('gulp-cssnano');
const concat = require('gulp-concat');
const clean = require('gulp-clean');
const imagemin = require('gulp-imagemin');
const changed = require('gulp-changed');
const browsersync = require('browser-sync').create();
const cleanCss = require('gulp-clean-css');
const flatmap = require('gulp-flatmap');
const usemin = require('gulp-usemin');
const rev = require('gulp-rev');
const htmlmin = require('gulp-htmlmin');

// Clean assets
function clear() {
    return src('dist/', {
        read: false,
        allowEmpty: true
    })
        .pipe(clean());
}

// JS function
function js() {
    const source = './js/*.js';
    return src(source)
        .pipe(changed(source))
        .pipe(concat('bundle.js'))
        .pipe(uglify())
        .pipe(rename({
            extname: '.min.js'
        }))
        .pipe(dest('./dist/js/'))
        .pipe(browsersync.stream());
}

// CSS function

function css() {
    const source = './css/*.scss';
    return src(source)
        .pipe(changed(source))
        .pipe(sass())
        .pipe(autoprefixer({
            overrideBrowserslist: ['last 2 versions'],
            cascade: false
        }))
        .pipe(rename({
            extname: '.min.css'
        }))
        .pipe(cssnano())
        .pipe(dest('./dist/css/'))
        .pipe(browsersync.stream());
}

// Optimize images

function img() {
    return src('./images/*')
        .pipe(imagemin())
        .pipe(dest('./dist/images'));
}

// Copyfonts

function copyfonts() {
    return src('./node_modules/open-iconic/font/fonts/*.{ttf,woff,eof,svg,eot,otf}*')
        .pipe(dest('./dist/fonts'))
}

// Optimize html
function html(){
    return src('./*.html')
        .pipe(flatmap(function(stream,filte){
            return stream
                .pipe(usemin({
                    css:[rev()],
                    html: [function(){ return htmlmin({collapseWhitespace: true})}],
                    js: [uglify(),rev()],
                    inlinejs: [uglify()],
                    inlinecss:[cleanCss(), 'concat']
                }));
        }))
        .pipe(dest('dist/'));
}

// Watch files
function watchFiles() {
    watch('./css/*', css);
    watch('./js/*', js);
    watch('./img/*', img);
    watch('./*.html', html);
}

// BrowserSync

function browserSync() {
    browsersync.init({
        server: {
            baseDir: './'
        },
        port: 3000
    });
}

// Tasks to define the execution of the functions simultaneously or in series

exports.default = parallel(watchFiles, browserSync);
exports.build = series(clear, parallel(js, css, img, html,copyfonts));



/*
'use strict';
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    uglify = require('gulp-uglify'),
    usemin = require('gulp-usemin'),
    rev = require('gulp-rev'),
    cleanCss = require('gulp-clean-css'),
    flatmap = require('gulp-flatmap'),
    htmlmin = require('gulp-htmlmin');

gulp.task('sass',function (){
    gulp.src('./css/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css'));
});

gulp.task('sass:watch',function (){
    gulp.watch('./css/*.scss',['sass']);
});

gulp.task('browser:sync',function (){
    var files =['./*.html','./css/*.css','./','./img/*.{png,jpg,gif}','./js/*.js']
    browserSync.init(files,{
        server:{
            baseDir:'./'
        }
    });
});

gulp.task('default',['sass'], function(){
    gulp.start('sass');
});


gulp.task('clean',function (){
    return del(['dist']);
});

gulp.task('copyfonts',function (){
    return gulp.src('./node_modules/open-iconic/font/fonts/*.{ttf,woff,eof,svg,eot,otf}*')
        .pipe(gulp.dest('./dist/fonts'))
});

gulp.task('imagemin',function (){
    return gulp.src('./images/*.{png,jpg,gif}')
        .pipe(imagemin({optimizationLevel: 3, progressive: true, interlaced: true}))
        .pipe(gulp.dest('dist/images'));
});

gulp.task('usemin',function (){
    return gulp.src('./*.html')
        .pipe(flatmap(function(stream,filte){
            return stream
                .pipe(usemin({
                    css:[rev()],
                    html: [function(){ return htmlmin({collapseWhitespace: true})}],
                    js: [uglify(),rev()],
                    inlinejs: [uglify()],
                    inlinecss:[cleanCss(), 'concat']
                }));
        }))
        .pipe(gulp.dest('dist/'));
});

gulp.task('build',['clean'],function () {
    gulp.start('copyfonts', 'imagemin', 'usemin');
})*/